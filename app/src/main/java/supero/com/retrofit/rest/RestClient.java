package supero.com.retrofit.rest;

import android.content.Context;

import retrofit2.Converter;
import retrofit2.Retrofit;
import supero.com.retrofit.rest.search.SearchGeo;

/**
 * Created by Carlos Lima on 1/27/2017.
 */
public class RestClient
{
    private SearchGeo apiSearchService;

    public RestClient(Context context)
    {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.geonames.org/")
                .build();

        apiSearchService = retrofit.create(SearchGeo.class);
    }

    public SearchGeo getApiSearchService()
    {
        return apiSearchService;
    }

}