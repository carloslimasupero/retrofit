package supero.com.retrofit.rest.search;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Carlos Lima on 1/27/2017.
 */
public interface SearchGeo {

    @Headers({"Accept:text/plain"})
    @POST("childrenJSON")
    public Call<ResponseBody> searchLocation(@Query("geonameId") String geonameid);
}
