package supero.com.retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import supero.com.retrofit.rest.RestClient;

public class MainActivity extends AppCompatActivity {

    TextView searchText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchText = (TextView) findViewById(R.id.searchResult);

        searchGeo();

    }

    protected void searchGeo()
    {
        RestClient service = new RestClient(this);

        Call<ResponseBody> call = service.getApiSearchService().searchLocation("3469034");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    searchText.setText(response.body().string().toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.getMessage();

                Log.v("LOG","Houve um erro");
            }

        });
    }

}
